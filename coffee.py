# coding: utf-8
import psycopg2
from flask import Flask, render_template, g, request, abort, redirect,\
                  url_for, session, escape, flash, make_response

from werkzeug.datastructures import MultiDict

from model import *
from forms import *
from utils import *
import config

import json

app = Flask(__name__)

@app.teardown_appcontext
def close_db(*args):
    if 'db' in g:
        g.db.close()

@app.route('/admin')
@admin_required
def admin():
    return render_template('admin.html')

@app.route('/admin/clients')
@admin_required
def admin_clients():
    clients = Client(get_db('manager'))
    return render_template('admin_clients.html', clients=clients.all())

@app.route('/admin/clients/delete/<client_id>')
@admin_required
def admin_delete_client(client_id):
    return generic_delete(Client(get_db('manager')),
                          u'Usunięto użytkownika.',
                          url_for('admin_clients'),
                          client_id)

@app.route('/admin/dealers')
@admin_required
def admin_dealers():
    dealers = Dealer(get_db('manager'))
    return render_template('admin_dealers.html', dealers=dealers.all())

@app.route('/admin/dealers/new', methods=['POST', 'GET'])
@admin_required
def admin_new_dealer():
    try:
        return generic_new(
            m = Dealer(get_db('manager')),
            msg = u'Dodano dostawcę.',
            redir = url_for('admin_dealers'),
            form = DealerForm(request.form),
            template = 'admin_new_dealer.html')
    except psycopg2.IntegrityError:
        flash(u'Dostawca o podanej nazwie już istnieje.')
        return redirect(url_for('admin_new_dealer'))

@app.route('/admin/dealers/edit/<dealer_id>', methods=['POST', 'GET'])
@admin_required
def admin_edit_dealer(dealer_id):
    try:
        return generic_edit(
            m = Dealer(get_db('manager')),
            msg = u'Uaktualniono dane dostawcy.',
            redir = url_for('admin_dealers'),
            object_id = dealer_id,
            form_ctor = DealerForm,
            template = 'admin_edit_dealer.html')
    except psycopg2.IntegrityError:
        flash(u'Dostawca o podanej nazwie już istnieje.')
        return redirect(url_for('admin_edit_dealer', dealer_id=dealer_id))

@app.route('/admin/dealers/delete/<dealer_id>')
@admin_required
def admin_delete_dealer(dealer_id):
    return generic_delete(Dealer(get_db('manager')),
                          u'Usunięto dostawcę.',
                          url_for('admin_dealers'),
                          dealer_id)

@app.route('/admin/elements')
@admin_required
def admin_elements():
    elements = Element(get_db('manager'))
    return render_template('admin_elements.html', elements=elements.all())

@app.route('/admin/elements/new', methods=['POST', 'GET'])
@admin_required
def admin_new_element():
    return generic_new(
        m = Element(get_db('manager')),
        msg = u'Dodano wartość kolekcjonerską.',
        redir = url_for('admin_elements'),
        form = ElementForm(request.form),
        template = 'admin_new_element.html')

@app.route('/admin/elements/delete/<element_id>')
@admin_required
def admin_delete_element(element_id):
    return generic_delete(
        m = Element(get_db('manager')),
        msg = u'Usunięto wartość kolekcjonerską.',
        redir = url_for('admin_elements'),
        object_id = element_id)

@app.route('/admin/elements/edit/<element_id>', methods=['POST', 'GET'])
@admin_required
def admin_edit_element(element_id):
    return generic_edit(
        m = Element(get_db('manager')),
        msg = u'Uaktualniono wartość kolekcjonerską.',
        redir = url_for('admin_elements'),
        object_id = element_id,
        form_ctor = ElementForm,
        template = 'admin_edit_element.html')

@app.route('/admin/orders')
@admin_required
def admin_orders():
    orders = Order(get_db('manager'))
    return render_template('admin_orders.html', orders=orders.all_with_items())

@app.route('/admin/orders/complete/<order_id>')
@admin_required
def admin_complete_order(order_id):
    orders = Order(get_db('manager'))
    order = exists_or_404(orders.get(order_id))

    orders.update(order_id, {'completed': True})
    flash(u'Zrealizowano zamówienie.')
    return redirect(url_for('admin_orders'))

@app.route('/admin/orders/delete/<order_id>')
@admin_required
def admin_delete_order(order_id):
    orders = Order(get_db('manager'))
    order = exists_or_404(orders.get(order_id))

    orders.delete(order_id)
    flash(u'Usunięto zamówienie.')
    return redirect(url_for('admin_orders'))

@app.route('/admin/products')
@admin_required
def admin_products():
    products = Product(get_db('manager')).all_with_elements()
    return render_template('admin_products.html', products=products)

@app.route('/admin/products/new', methods=['POST', 'GET'])
@admin_required
def admin_new_product():
    products = Product(get_db('manager'))
    dealers = Dealer(get_db('manager'))
    choices = [ (d['dealer_id'], d['name']) for d in dealers.all() ]

    form = AddProductForm(request.form)
    form.dealer_id.choices = choices
    if should_render(request, form):
        return render_template('admin_new_product.html', form=form)

    products.insert(form.data)
    flash(u'Dodano produkt.')
    return redirect(url_for('admin_products'))

@app.route('/admin/products/edit/<product_id>', methods=['POST', 'GET'])
@admin_required
def admin_edit_product(product_id):
    products = Product(get_db('manager'))
    product = dict(products.get(product_id))

    for key in ['mass', 'volume']:
        if product[key] is None:
            del product[key]

    form = ProductForm(request.form)
    if should_render(request, form):
        if request.method == 'GET':
            form = ProductForm(tomulti(product))

        return render_template('admin_edit_product.html', form=form, product=product)

    products.update(product_id, form.data)
    flash(u'Uaktualniono dane produktu.')
    return redirect(url_for('admin_products'))

@app.route('/admin/products/delete/<product_id>')
@admin_required
def admin_delete_product(product_id):
    products = Product(get_db('manager'))
    try:
        products.delete(product_id)
    except psycopg2.Error:
        flash(u'Wystąpił błąd podczas usuwania produktu.')
    else:
        flash(u'Usunięto produkt.')

    return redirect(url_for('admin_products'))

@app.route('/admin/product-element/delete/<element_id>/<product_id>')
@admin_required
def admin_delete_product_element(element_id, product_id):
    elements = Element(get_db('manager'))
    element_of_product = exists_or_404(elements.get_element_of_product(element_id, product_id))

    elements.delete_element_of_product(element_id, product_id)
    flash(u'Usunięto wartość kolekcjonerską produktu.')
    return redirect(url_for('admin_products'))

@app.route('/admin/product-element/edit/<element_id>/<product_id>', methods=['POST', 'GET'])
@admin_required
def admin_edit_product_element(element_id, product_id):
    elements = Element(get_db('manager'))
    products = Product(get_db('manager'))
    element_of_product = exists_or_404(elements.get_element_of_product(element_id, product_id))
    product = products.get(product_id)
    element = elements.get(element_id)

    form = ElementOfProductForm(request.form)
    if should_render(request, form):
        if request.method == 'GET':
            form = ElementOfProductForm(tomulti(element_of_product))

        return render_template('admin_edit_element_of_product.html', form=form, product=product, element=element)

    elements.update_element_of_product(element_id, product_id, form.data)
    flash(u'Uaktualniono wartość kolekcjonerską produktu.')
    return redirect(url_for('admin_products'))

@app.route('/admin/product-element/new/<product_id>', methods=['POST', 'GET'])
@admin_required
def admin_new_product_element(product_id):
    elements = Element(get_db('manager'))
    products = Product(get_db('manager'))
    product = products.get(product_id)
    all_elements = elements.all()

    form = AddElementOfProductForm(request.form)
    form.element_id.choices = [ (e['element_id'], e['name']) for e in all_elements ]
    if should_render(request, form):
        return render_template('admin_new_element_of_product.html', form=form, product=product)

    elements.insert_element_of_product(form.data['element_id'], product_id, form.data)
    flash(u'Dodano wartość kolekcjonerską produktu.')
    return redirect(url_for('admin_products'))

@app.route('/')
def index():
    products = BareProduct(get_db())
    return render_template('index.html', products=products.all_with_elements())

@app.route('/order/start', methods=['POST'])
@client_required
def order_start():
    items = json.loads(request.form['payload'])
    products = BareProduct(get_db('client'))

    order_products = {}
    for item in items:
        product = exists_or_404(products.get(item['key']))
        order_products[product['product_id']] = product

    return render_template('order_start.html', payload=request.form['payload'], products=order_products)

@app.route('/order/save', methods=['POST'])
@client_required
def order_save():
    items = json.loads(request.form['payload'])
    orders = Order(get_db('client'))
    clients = Client(get_db('client'))

    client = exists_or_404(clients.get_by_login(session['login']))
    address = None
    if request.form['address']:
        address = request.form['address']

    try:
        orders.create_order(client['client_id'], address, items)
    except psycopg2.InternalError:
        flash(u'Niewystarczająca licza produktów w magazynie.')
        return redirect('/')

    flash(u'Zamówienie przekazano do realizacji.')
    resp = redirect('/')
    resp.set_cookie('%s_cart' % session['login'], '[]');
    return resp

@app.route('/profile', methods=['POST', 'GET'])
@client_required
def profile():
    clients = Client(get_db('client'))
    client = exists_or_404(clients.get_by_login(session['login']))
    client['password'] = ''

    form = ClientForm(request.form)
    if should_render(request, form):
        if request.method == 'GET':
            form = ClientForm(tomulti(client))

        return render_template('profile.html', form=form)

    data = dict(form.data)
    if not data['password']:
        del data['password']

    email_client = clients.get_by('email', data['email'])
    if email_client and email_client['client_id'] != client['client_id']:
        flash(u'Podany email jest już zajęty.')
    else:
        clients.update(client['client_id'], data)
        flash(u'Uaktualniono profil.')

    return redirect(url_for('profile'))

@app.route('/orders')
@client_required
def orders():
    orders = Order(get_db('client'))
    clients = Client(get_db('client'))

    client = exists_or_404(clients.get_by_login(session['login']))
    return render_template('orders.html', orders=orders.all_with_items_by_client(client['client_id'], 'BareProduct'))

@app.route('/elements')
def elements():
    elements = Element(get_db())
    return render_template('elements_list.html', elements=elements.all())

@app.route('/elements/<id>')
def products_by_element(id):
    products = BareProduct(get_db())
    elements = Element(get_db())
    return render_template('products_list.html',
            products=products.by_element(id),
            element=exists_or_404(elements.get(id)))

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        return render_template('login.html')

    if request.form['login'] == 'admin':
        if request.form['password'] == config.ADMIN_PASSWORD:
            flash(u'Zalogowano administratora.')
            session['login'] = 'admin'
            session['admin'] = True
            return redirect(url_for('admin'))
        else:
            flash(u'Niepoprawne dane.')
            return redirect(url_for('login'))

    clients = Client(get_db('client'))
    client = clients.authorize(request.form['login'], request.form['password'])
    if not client:
        flash(u'Niepoprawne dane.')
        return redirect(url_for('login'))

    flash(u'Zalogowano.')
    session['login'] = client['login']
    return redirect(url_for('index'))

@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegistrationForm(request.form)
    if should_render(request, form):
        return render_template('register.html', form=form)

    clients = Client(get_db('client'))
    if clients.get_by('login', form.data['login']):
        flash(u'Użytkownik o podanym loginie już istnieje.')
        return redirect(url_for('register'))

    if clients.get_by('email', form.data['email']):
        flash(u'Użytkownik o podanym emailu już istnieje.')
        return redirect(url_for('register'))

    clients.insert(form.data)
    flash(u'Zarejestrowano.')
    return redirect(url_for('login'))

@app.route('/logout')
def logout():
    session['login'] = session['admin'] = None
    flash(u'Wylogowano.')
    return redirect(url_for('index'))

app.secret_key = '8b76gesrnqegz8gigygfspdfa97ewfg'
if __name__ == "__main__":
    app.run(debug=True)

