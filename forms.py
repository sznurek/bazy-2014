# coding: utf-8

from wtforms import form, fields, validators

def should_render(request, form):
    return request.method == 'GET' or (request.method == 'POST' and not form.validate())

class BaseForm(form.Form):
    class Meta:
        locales = ['pl_PL', 'pl']

class ClientForm(BaseForm):
    password = fields.PasswordField(u'Hasło', [validators.optional()])
    name = fields.StringField(u'Imię', [validators.required()])
    surname = fields.StringField(u'Nazwisko', [validators.required()])
    email = fields.StringField(u'Adres email', [validators.required()])
    phone_number = fields.StringField(u'Numer telefonu', [
        validators.optional(),
        validators.length(min=9)])
    address = fields.TextField(u'Adres', [validators.required()])

class RegistrationForm(ClientForm):
    login = fields.StringField(u'Login', [
        validators.required(),
        validators.length(min=3, max=10)])
    password = fields.PasswordField(u'Hasło', [validators.required()])

class DealerForm(BaseForm):
    name = fields.StringField(u'Nazwa', [validators.required()])
    phone_number = fields.StringField(u'Telefon', [
        validators.required(),
        validators.length(min=9)])
    address = fields.StringField(u'Adres', [validators.required()])

class ElementForm(BaseForm):
    name = fields.StringField(u'Nazwa', [validators.required()])
    description = fields.TextField(u'Opis', [validators.required()])

class ElementOfProductForm(BaseForm):
    percent = fields.FloatField(u'Zawartość procentowa', [
        validators.required(),
        validators.NumberRange(min=0.0, max=1.0)])
    measurement_unit = fields.SelectField(u'Jednostka',
        choices=[('m', u'Masa'), ('v', u'Objętość')])

class AddElementOfProductForm(ElementOfProductForm):
    element_id = fields.SelectField(u'Wartość kolekcjonerska', coerce=int)

def product_mass_volume_validator(form, field):
    print 'Fire %s %s' % (str(field.data), str(form['volume'].data))
    if form['mass'].data is None and form['volume'].data is None:
        print 'BOOM'
        field.errors[:] = []
        raise validators.StopValidation(u'Należy podać masę lub objętość.')

    if field.data is None:
        field.errors[:] = []
        raise validators.StopValidation()

class ProductForm(BaseForm):
    name = fields.StringField(u'Nazwa', [validators.required()])
    description = fields.StringField(u'Opis', [validators.required()])
    price = fields.IntegerField(u'Cena (w groszach)', [
        validators.required(),
        validators.NumberRange(min=0)])
    dealer_price = fields.IntegerField(u'Cena hurtowa (w groszach)', [
        validators.required(),
        validators.NumberRange(min=0)])
    units_available = fields.IntegerField(u'Dostępne jednostki', [
        validators.required(),
        validators.NumberRange(min=0)])
    mass = fields.FloatField(u'Masa (mg)', [
        validators.NumberRange(min=0),
        product_mass_volume_validator])
    volume = fields.FloatField(u'Objętość (cm^3)', [
        validators.NumberRange(min=0),
        product_mass_volume_validator])

class AddProductForm(ProductForm):
    dealer_id = fields.SelectField(u'Dostawca', coerce=int)

