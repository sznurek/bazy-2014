DROP TABLE IF EXISTS Dealer CASCADE;
CREATE TABLE Dealer (
    dealer_id serial primary key,
    name varchar NOT NULL UNIQUE,
    address varchar NOT NULL,
    phone_number varchar
);
CREATE INDEX ON Dealer (name);

DROP TABLE IF EXISTS Product CASCADE;
CREATE TABLE Product (
    product_id serial primary key,
    dealer_id int NOT NULL references Dealer on delete cascade,
    name varchar NOT NULL,
    description varchar NOT NULL,
    price int NOT NULL, -- in grosze
    dealer_price int NOT NULL, -- in grosze
    units_available int NOT NULL DEFAULT 0,
    mass real, -- in grams
    volume real, -- in cm^3

    CHECK (mass IS NOT NULL OR volume IS NOT NULL)
);
CREATE INDEX ON Product (dealer_id);

DROP TABLE IF EXISTS Element CASCADE;
CREATE TABLE Element (
    element_id serial primary key,
    name varchar NOT NULL,
    description varchar NOT NULL
);

DROP DOMAIN IF EXISTS unit CASCADE;
CREATE DOMAIN unit char(1) CHECK (VALUE IN ('m', 'v'));

DROP TABLE IF EXISTS ElementOfProduct CASCADE;
CREATE TABLE ElementOfProduct (
    element_id int NOT NULL references Element on delete cascade,
    product_id int NOT NULL references Product on delete cascade,
    percent real,
    measurement_unit unit NOT NULL,

    PRIMARY KEY (element_id, product_id)
);

DROP TABLE IF EXISTS Client CASCADE;
CREATE TABLE Client (
    client_id serial primary key,
    login varchar NOT NULL UNIQUE,
    password varchar NOT NULL,
    name varchar NOT NULL,
    surname varchar NOT NULL,
    address varchar NOT NULL,
    phone_number varchar,
    email varchar NOT NULL UNIQUE
);
CREATE INDEX ON Client (login);
CREATE INDEX ON Client (email);

DROP TABLE IF EXISTS ClientOrder CASCADE;
CREATE TABLE ClientOrder (
    order_id serial primary key,
    client_id int NOT NULL references Client on delete CASCADE,
    order_date timestamp NOT NULL,
    shipping_address varchar, -- NULL -> take Client's address
    completed boolean NOT NULL DEFAULT false
);
CREATE INDEX ON ClientOrder (client_id);

DROP TABLE IF EXISTS ClientOrderItem CASCADE;
CREATE TABLE ClientOrderItem (
    order_id int NOT NULL references ClientOrder on delete cascade,
    product_id int NOT NULL references Product on delete cascade,
    quantity int NOT NULL DEFAULT 1,

    PRIMARY KEY (order_id, product_id)
);

-- Trigger:
-- Act of ordering a product descreases its quantity.
CREATE OR REPLACE FUNCTION order_item_change() RETURNS TRIGGER AS
$end$
DECLARE
    product_qty integer;
BEGIN
    product_qty := (SELECT units_available FROM Product WHERE product_id = NEW.product_id);

    IF (TG_OP = 'INSERT' AND product_qty < NEW.quantity) THEN
        RAISE 'Not enough units';
    END IF;

    IF (TG_OP = 'UPDATE' AND OLD.product_id = NEW.product_id AND product_qty + OLD.quantity < NEW.quantity) THEN
        RAISE 'Not enough units';
    END IF;

    IF (TG_OP = 'UPDATE') THEN
        UPDATE Product SET units_available = units_available + OLD.quantity WHERE product_id = OLD.product_id;
    END IF;

    UPDATE Product SET units_available = units_available - NEW.quantity WHERE product_id = NEW.product_id;

    RETURN NEW;
END;
$end$ LANGUAGE plpgsql SECURITY DEFINER;

CREATE OR REPLACE FUNCTION order_item_delete() RETURNS TRIGGER AS
$end$
BEGIN
    UPDATE Product SET units_available = units_available + OLD.quantity WHERE product_id = OLD.product_id;
    RETURN NEW;
END;
$end$ LANGUAGE plpgsql SECURITY DEFINER;

DROP TRIGGER IF EXISTS product_qty_decrease ON ClientOrderItem CASCADE;
CREATE TRIGGER product_qty_decrease
    BEFORE INSERT OR UPDATE ON ClientOrderItem
    FOR EACH ROW
    EXECUTE PROCEDURE order_item_change();

DROP TRIGGER IF EXISTS product_qty_increace ON ClientOrderItem CASCADE;
CREATE TRIGGER product_qty_increace
    BEFORE DELETE ON ClientOrderItem
    FOR EACH ROW
    EXECUTE PROCEDURE order_item_delete();

-- Products view for users (with sensitive data stripped off).
CREATE OR REPLACE VIEW BareProduct
    (product_id, name, description, price, units_available, mass, volume) AS
    SELECT product_id, name, description, price, units_available, mass, volume FROM Product;

-- Roles and permissions

REVOKE ALL ON Dealer FROM PUBLIC;
REVOKE ALL ON Product FROM PUBLIC;
REVOKE ALL ON Element FROM PUBLIC;
REVOKE ALL ON ElementOfProduct FROM PUBLIC;
REVOKE ALL ON Client FROM PUBLIC;
REVOKE ALL ON ClientOrder FROM PUBLIC;
REVOKE ALL ON ClientOrderItem FROM PUBLIC;

DROP USER IF EXISTS GUEST;
CREATE ROLE GUEST LOGIN ENCRYPTED PASSWORD 'guest';
GRANT SELECT ON BareProduct, Element, ElementOfProduct TO GUEST;
GRANT INSERT ON Client TO GUEST;

DROP USER IF EXISTS CLIENT;
CREATE ROLE CLIENT LOGIN ENCRYPTED PASSWORD 'client';
GRANT SELECT ON BareProduct, Element, ElementOfProduct TO CLIENT;
GRANT INSERT, UPDATE, SELECT ON Client TO CLIENT;
GRANT INSERT, UPDATE, SELECT ON ClientOrder, ClientOrderItem TO CLIENT;

DROP USER IF EXISTS MANAGER;
CREATE ROLE MANAGER LOGIN ENCRYPTED PASSWORD 'manager';
GRANT ALL PRIVILEGES ON Dealer, Product, Element, ElementOfProduct, Client, ClientOrder, ClientOrderItem TO MANAGER;

GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO GUEST, CLIENT, MANAGER;
