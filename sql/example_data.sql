--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Data for Name: client; Type: TABLE DATA; Schema: public; Owner: sznurek
--

COPY client (client_id, login, password, name, surname, address, phone_number, email) FROM stdin;
1	janusz	9d4e1e23bd5b727046a9e3b4b7db57bd8d6ee684	Janusz	Testowy	Nieistniejąca 7/10, 58-765 Obora	555666777	janusz@poczta.pl
2	alicja	9d4e1e23bd5b727046a9e3b4b7db57bd8d6ee684	Alicja	Przykładowa	Bajkowa 13, 34-189 Nibylandia	333444555	alicja@poczta.pl
\.


--
-- Name: client_client_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sznurek
--

SELECT pg_catalog.setval('client_client_id_seq', 2, true);


--
-- Data for Name: clientorder; Type: TABLE DATA; Schema: public; Owner: sznurek
--

COPY clientorder (order_id, client_id, order_date, shipping_address, completed) FROM stdin;
\.


--
-- Name: clientorder_order_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sznurek
--

SELECT pg_catalog.setval('clientorder_order_id_seq', 1, false);


--
-- Data for Name: dealer; Type: TABLE DATA; Schema: public; Owner: sznurek
--

COPY dealer (dealer_id, name, address, phone_number) FROM stdin;
1	Dostawca 1	Adres dostawcy 1	666777888
2	Dostawca 2	Adres dostawcy 2	666777889
3	Dostawca 3	Adres dostawcy 3	666777900
\.


--
-- Data for Name: product; Type: TABLE DATA; Schema: public; Owner: sznurek
--

COPY product (product_id, dealer_id, name, description, price, dealer_price, units_available, mass, volume) FROM stdin;
1	1	Produkt 1	Opis produktu 1	3000	2500	100	1000	\N
2	2	Produkt 2	Opis produktu 2	5000	4300	54	\N	1226
3	3	Produkt 3	Opis produktu 3	7800	7000	55	556	\N
4	2	Produkt 4	Opis produktu 4	1000	700	1000	1000	1000
\.


--
-- Data for Name: clientorderitem; Type: TABLE DATA; Schema: public; Owner: sznurek
--

COPY clientorderitem (order_id, product_id, quantity) FROM stdin;
\.


--
-- Name: dealer_dealer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sznurek
--

SELECT pg_catalog.setval('dealer_dealer_id_seq', 3, true);


--
-- Data for Name: element; Type: TABLE DATA; Schema: public; Owner: sznurek
--

COPY element (element_id, name, description) FROM stdin;
1	Wartość kolekcjonerska 1	Opis wartości kolekcjonerskiej 1
2	Wartość kolekcjonerska 2	Opis wartości kolekcjonerskiej 2
3	Wartość kolekcjonerska 3	Opis wartości kolekcjonerskiej 3
4	Wartość kolekcjonerska 4	Opis wartości kolekcjonerskiej 4
\.


--
-- Name: element_element_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sznurek
--

SELECT pg_catalog.setval('element_element_id_seq', 4, true);


--
-- Data for Name: elementofproduct; Type: TABLE DATA; Schema: public; Owner: sznurek
--

COPY elementofproduct (element_id, product_id, percent, measurement_unit) FROM stdin;
1	1	0.119999997	m
4	1	0.0299999993	v
2	2	0.400000006	m
3	3	0.119999997	m
4	3	0.109999999	m
2	3	0.00999999978	v
2	4	0.00999999978	m
\.


--
-- Name: product_product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: sznurek
--

SELECT pg_catalog.setval('product_product_id_seq', 4, true);


--
-- PostgreSQL database dump complete
--

