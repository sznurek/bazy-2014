# coding: utf-8
import psycopg2
from flask import Flask, render_template, g, request, abort, redirect,\
                  url_for, session, escape, flash, make_response

from werkzeug.datastructures import MultiDict

from functools import wraps
import config

from forms import *

def get_db(user='guest'):
    if not '%s_db' % user in g:
        setattr(g, '%s_db' % user, psycopg2.connect(database=config.DB_NAME, user=user, password=config.DB_USERS[user]))

    return g.get('%s_db' % user)

def dictify(form):
    result = {}
    for (k, v) in dict(form).items():
        result[k] = v[0]
    return result

def tomulti(d):
    return MultiDict(d.iteritems())

def exists_or_404(obj):
    if not obj:
        abort(404)
    return obj

def client_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if not 'login' in session or session['login'] is None:
            flash(u'Ta strona wymaga zalogowania się.')
            return redirect(url_for('login'))
        return f(*args, **kwargs)
    return decorated

def admin_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if not 'admin' in session or session['admin'] is None:
            flash(u'Ta strona wymaga uprawnień administratora.')
            return redirect(url_for('login'))
        return f(*args, **kwargs)
    return decorated

# Generic controllers
def generic_delete(m, msg, redir, object_id):
    exists_or_404(m.get(object_id))
    m.delete(object_id)
    flash(msg)
    return redirect(redir)

def generic_new(m, msg, redir, form, template):
    if should_render(request, form):
        return render_template(template, form=form)

    m.insert(form.data)
    flash(msg)
    return redirect(redir)

def generic_edit(m, msg, redir, object_id, form_ctor, template):
    obj = exists_or_404(m.get(object_id))

    form = form_ctor(request.form)
    if should_render(request, form):
        if request.method == 'GET':
            form = form_ctor(tomulti(obj))

        return render_template(template, form=form, obj=obj)

    m.update(object_id, form.data)
    flash(msg)
    return redirect(redir)

