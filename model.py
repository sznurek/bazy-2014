# coding: utf-8

import psycopg2
from psycopg2.extras import DictCursor

from sha import sha

class Model(object):
    def __init__(self, db):
        self.db = db

    # To be overriden
    def key_name(self):
        return self.TABLE.lower() + '_id'

    def cursor(self):
        cur = self.db.cursor(cursor_factory=DictCursor)
        psycopg2.extensions.register_type(psycopg2.extensions.UNICODE, cur)
        return cur

    def fetchall(self, sql, *args):
        cur = self.cursor()
        cur.execute(sql, *args)

        self.db.commit()
        return cur.fetchall()

    def fetchone(self, sql, *args):
        cur = self.cursor()
        cur.execute(sql, *args)

        self.db.commit()
        return cur.fetchone()

    def execute(self, sql, *args):
        cur = self.cursor()
        cur.execute(sql, *args)

        self.db.commit()

    def all(self):
        return self.fetchall('SELECT * FROM %s' % self.TABLE)

    def all_by(self, column, value):
        sql = 'SELECT * FROM %s WHERE %s = %%s' % (self.TABLE, column)
        return self.fetchall(sql, [value])

    def get_by(self, column, value):
        sql = 'SELECT * FROM %s WHERE %s = %%s' % (self.TABLE, column)
        return self.fetchone(sql, [value])

    def get(self, object_id):
        return self.get_by(self.key_name(), object_id)

    def delete(self, object_id):
        sql = 'DELETE FROM %s WHERE %s = %%s' % (self.TABLE, self.key_name())
        self.execute(sql, [object_id])

    def insert(self, data):
        columns = ', '.join(data.keys())
        placeholders = ', '.join(map(lambda x: "%(" + x + ")s", data.keys()))
        sql = 'INSERT INTO %s (%s) VALUES (%s)' % (self.TABLE, columns, placeholders)

        self.execute(sql, data)

    def update(self, object_id, data):
        new_values = ', '.join(['%s = %%(%s)s' % (x, x) for x in data.keys()])
        sql = 'UPDATE %s SET %s WHERE %s = %%(%s)s' % (self.TABLE, new_values,
              self.key_name(), self.key_name())

        update_data = dict(data)
        update_data[self.key_name()] = object_id

        self.execute(sql, update_data)

class BareProduct(Model):
    TABLE = 'BareProduct'

    def key_name(self):
        return 'product_id'

    def _add_elements(self, products):
        elements = Element(self.db)

        products = map(dict, products)
        for product in products:
            product['elements'] = elements.for_product(product['product_id'])

        return products

    def all_with_elements(self):
        return self._add_elements(self.all())

    def by_element(self, element_id):
        sql = 'SELECT * FROM %s bp WHERE EXISTS \
               (SELECT * FROM ElementOfProduct WHERE element_id = %%s AND\
               product_id = bp.product_id)' % self.TABLE

        return self._add_elements(self.fetchall(sql, [element_id]))

class Product(BareProduct):
    TABLE = 'Product'

    def all_with_elements(self):
        sql = 'SELECT p.*, d.name AS dealer_name FROM Product p JOIN Dealer d \
               ON p.dealer_id = d.dealer_id'

        return self._add_elements(self.fetchall(sql))

class Element(Model):
    TABLE = 'Element'
    def for_product(self, product_id):
        sql = 'SELECT e.*, ep.* FROM ElementOfProduct ep\
               JOIN Element e ON ep.element_id = e.element_id\
               WHERE ep.product_id = %s'

        return self.fetchall(sql, [product_id])

    def get_element_of_product(self, element_id, product_id):
        sql = 'SELECT * FROM ElementOfProduct WHERE element_id = %s AND product_id = %s'
        return self.fetchone(sql, [element_id, product_id])

    def delete_element_of_product(self, element_id, product_id):
        sql = 'DELETE FROM ElementOfProduct WHERE element_id = %s AND product_id = %s'
        self.execute(sql, [element_id, product_id])

    def update_element_of_product(self, element_id, product_id, data):
        sql = 'UPDATE ElementOfProduct SET percent = %s, measurement_unit = %s \
               WHERE element_id = %s AND product_id = %s'

        self.execute(sql, [data['percent'], data['measurement_unit'], element_id, product_id])

    def insert_element_of_product(self, element_id, product_id, data):
        sql = 'INSERT INTO ElementOfProduct (percent, measurement_unit, \
               element_id, product_id) VALUES (%s, %s, %s, %s)'

        self.execute(sql, [data['percent'], data['measurement_unit'], element_id, product_id])

class Dealer(Model):
    TABLE = 'Dealer'

class Client(Model):
    TABLE = 'Client'
    def hash(self, password):
        return sha(password).hexdigest()

    def insert(self, data):
        to_insert = dict(data)
        to_insert['password'] = self.hash(to_insert['password'])

        super(Client, self).insert(to_insert)

    def update(self, client_id, data):
        to_update = dict(data)
        if 'password' in to_update.keys():
            to_update['password'] = self.hash(to_update['password'])

        super(Client, self).update(client_id, to_update)

    def get_by_login(self, login):
        return self.get_by('login', login)

    def authorize(self, login, password):
        client = self.get_by_login(login)
        if not client:
            return None

        if self.hash(password) != client['password']:
            return None

        return client

class Order(Model):
    TABLE = 'ClientOrder'

    def key_name(self):
        return 'order_id'

    def all_with_items_by_client(self, client_id, table='Product'):
        sql = 'SELECT * FROM ClientOrder o JOIN ClientOrderItem oi\
               ON o.order_id = oi.order_id \
               JOIN %s p ON p.product_id = oi.product_id \
               WHERE o.client_id = %%s' % table

        rows = self.fetchall(sql, [client_id])
        return self._dictify_order_join(rows)

    def all_with_items(self):
        sql = 'SELECT o.*, oi.*, p.*, c.name AS client_name, c.surname AS client_surname, \
               c.login AS client_login, c.email AS client_email, c.address AS client_address, \
               c.phone_number AS client_phone_number FROM ClientOrder o JOIN ClientOrderItem oi \
               ON o.order_id = oi.order_id \
               JOIN Product p ON p.product_id = oi.product_id \
               JOIN Client c ON c.client_id = o.client_id'

        rows = self.fetchall(sql)
        return self._dictify_order_join(rows)

    def _dictify_order_join(self, rows):
        data = {}

        for row in rows:
            if not row['order_id'] in data.keys():
                data[row['order_id']] = {'order': row, 'items': []}

            data[row['order_id']]['items'].append(row)

        return data

    def create_order(self, client_id, address, products):
        from datetime import datetime

        cur = self.cursor()
        try:
            cur.execute("BEGIN")
            sql = 'INSERT INTO ClientOrder (client_id, order_date, shipping_address) \
                   VALUES (%s, %s, %s) RETURNING order_id'

            cur.execute(sql, [client_id, datetime.now(), address])
            order_id = cur.fetchone()[0]

            for product in products:
                sql = 'INSERT INTO ClientOrderItem (order_id, product_id, quantity) \
                       VALUES (%s, %s, %s)'
                cur.execute(sql, [order_id, int(product['key']), int(product['qty'])])

            cur.execute("COMMIT")
        except Exception:
            cur.execute("ROLLBACK")
            raise

