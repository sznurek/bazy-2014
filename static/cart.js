/** @jsx React.DOM */

var Cart = React.createClass({
    getInitialState: function() {
        return {items: []};
    },

    componentWillMount: function() {
        this.loadState();
    },

    componentDidMount: function() {
        var cart = this;

        $('.cart-add-link').on('click', function(element) {
            cart.addItem({
                key: $(this).data('key'),
                name: $(this).data('name'),
                price: $(this).data('price'),
                qty: 1
            });

            return false;
        });
    },

    componentDidUpdate: function() {
        this.saveState();
        $('.payload').val(JSON.stringify(this.state.items));
    },

    loadState: function() {
        var cookie = $.cookie(window.login + '_cart');
        if(cookie === undefined) {
            return;
        }

        this.setState({'items': cookie});
    },

    saveState: function() {
        $.cookie(window.login + '_cart', this.state.items, { path: '/' });
    },

    addItem: function(newItem) {
        var modified = false;
        var cart = this;
        $.each(this.state.items, function(i, item) {
            if(item.key == newItem.key) {
                cart.updateQty(item.key, item.qty + newItem.qty);
                modified = true;
            }
        });

        if(!modified) {
            var newState = React.addons.update(this.state,
                {items: {$push: [newItem]}});
            this.setState(newState);
        }
    },

    updateQty: function(key, qty) {
        var newState = this.state;
        newState.items = newState.items.map(function(item) {
            if(item.key !== key) {
                return item;
            }

            return $.extend(item, {qty: qty});
        });

        this.setState(newState);
    },

    countItems: function() {
        var count = 0;
        $.each(this.state.items, function(i, item) {
            count += item.qty;
        });
        return count;
    },

    totalPrice: function() {
        var price = 0;
        $.each(this.state.items, function(i, item) {
            price += item.price * item.qty;
        });
        return price;
    },

    renderTotalPrice: function() {
        return (this.totalPrice() / 100).toFixed(2).replace('.', ',') + ' zł';
    },

    render: function() {
        var cart = this;

        var cartItems = this.state.items.map(function(item) {
            return <CartItem key={item.key}
                             qty={item.qty}
                             name={item.name}
                             price={item.price}
                             updateQty={cart.updateQty} />
        });

        var form = null;
        if(window.hideCartSubmit === undefined) {
            form = <OrderForm items={this.state.items} />;
        }

        return (<span>Aktualnie masz <strong>{this.countItems()} </strong>
            produktów w koszyku łącznie za {this.renderTotalPrice()}.
            <ul>
                {cartItems}
            </ul>
            {form}
            </span>);
    }
});

var OrderForm = React.createClass({
    render: function() {
        return (
            <form action="/order/start" method="POST">
                <input type="hidden" name="payload" value={JSON.stringify(this.props.items)} />
                <input type="submit" value="Złóż zamówienie"/>
            </form>);
    }
});

var CartItem = React.createClass({
    renderPrice: function() {
        return (this.props.price * this.props.qty / 100).toFixed(2).replace('.', ',') + ' zł';
    },

    handleChange: function(event) {
        this.props.updateQty(this.props.key, parseInt(event.target.value, 10));
    },

    render: function() {
        var qtyInput = <input type="text" size="3" value={this.props.qty} onChange={this.handleChange} />
        return (<li key={this.props.key}>{this.props.name} (x{qtyInput}) - {this.renderPrice()}</li>);
    }
});

$(document).ready(function() {
    $.cookie.json = true;
    if(!window.hide_cart) {
        React.renderComponent(<Cart />, document.getElementById('cart'));
    }
});

